﻿<#
.Synopsis
   Add App Pools to certificate
.DESCRIPTION
   Add array of App Pools to a private key certificate identified by Friendly Name
.EXAMPLE
    Add-CertPermissions -FriendlyName OSI_BDI
.EXAMPLE
   Add-CertPermissions -FriendlyName OSI_BDI -AppPools SCU40BANK
#>
function Add-CertPermissions
{
    [CmdletBinding()]
    [OutputType([int])]
    Param
    (
        # friendly name of certificate
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        $FriendlyName,

        # App Pool name to add to the certificate
        [Parameter(Mandatory=$false,ValueFromPipelineByPropertyName=$true,ValueFromPipeline=$true)]
        [string[]] $AppPools,
        
        #The path to the csv file. The file is just a list of app pools
        [Parameter(Mandatory=$false,ValueFromPipelineByPropertyName=$true,ValueFromPipeline=$true)]
        [string]$path
    )
    
    Process
    {
       #pull thumbprint
       $thumbprint = ls Cert:\LocalMachine\My | 
        where {$_.FriendlyName -like $FriendlyName} | 
        select -ExpandProperty Thumbprint

       $cert = gci $thumbprint

       $cert.FriendlyName = 
       
       $fileName = ls Cert:\LocalMachine\My | 
        where {$_.FriendlyName -like $FriendlyName} | 
        select Subject,@{n='FileName';e={$_.PrivateKey.CspKeyContainerInfo.UniqueKeyContainerName}} | 
        select -ExpandProperty FileName
        if ($PSBoundParameters.ContainsKey('AppPools'))
        {
            foreach($AppPool in $AppPools){
                Invoke-Expression "icacls C:\ProgramData\Microsoft\crypto\rsa\machinekeys\$fileName /grant 'IIS AppPool\$($AppPool):F'" 
            }
        } else {
            Import-Csv $path -Header Pools | ForEach-Object {Invoke-Expression "icacls C:\ProgramData\Microsoft\crypto\rsa\machinekeys\$fileName /grant 'IIS AppPool\$($_.Pools):F'" }

        }
        

    }
   
}

<#
.Synopsis
   Add certificate to store
.DESCRIPTION
   Add certificate to store
.EXAMPLE
   Add-Certificate -Path 'C:\PowerShell Scripts\test.pfx'  -Password test

#>
function Add-Certificate {
    [CmdletBinding()]
    param
    (
        [Parameter(Position=1, Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$Path,

        [Parameter(Position=2, Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$Password
    )

    Write-Verbose -Message ('Installing certificate from path: {0}' -f $Path);

    try 
    {
        # Create the certificate
        $pfxcert = New-Object -TypeName System.Security.Cryptography.X509Certificates.X509Certificate2 -ErrorAction Stop;
        $KeyStorageFlags = [System.Security.Cryptography.X509Certificates.X509KeyStorageFlags]::Exportable -bxor [System.Security.Cryptography.X509Certificates.X509KeyStorageFlags]::MachineKeySet -bxor [System.Security.Cryptography.X509Certificates.X509KeyStorageFlags]::PersistKeySet;
        Write-Verbose ('Key storage flags is: {0}' -f $KeyStorageFlags);
        $pfxcert.Import($Path, $Password, $KeyStorageFlags);

        # Create the X509 store and import the certificate
        $store = New-Object -TypeName System.Security.Cryptography.X509Certificates.X509Store -ArgumentList My, LocalMachine -ErrorAction Stop;
        $store.Open([System.Security.Cryptography.X509Certificates.OpenFlags]::ReadWrite);
        $store.Add($pfxcert);
        $store.Close();

        Write-Output -InputObject $pfxcert;
    }
    catch 
    {
        throw $_;
    }
}