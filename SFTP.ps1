﻿function Invoke-SFTPMessage
{
<#
.Synopsis
   Send secure email via filetransfer.xxxx.com
.DESCRIPTION
   Long description
.PARAMETERS Email
    The email address to deliver the message
.EXAMPLE
   Invoke-SFTPMessage -Email dpeek -Subject "Solidarity files" -Body "Solidarity Setting Files" -Attachment "C:\inetpub\Sites\SOL40Bank\scripts.zip"

#>
    [CmdletBinding()]
    Param
    (
        # Param1 help description
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        $Email,
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=1)]
        $Subject,
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=2)]
        $Body,
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=3)]
        $Attachment
    )

    Begin{
        $oAPI = New-Object -ComObject MOVEitAPI.clientobj
        $oMsg = New-Object -ComObject MOVEitAPI.MOVEitMessageInfo
        $Credential = Get-Credential -Message "Enter your name and password for filetransfer.opensolutions.com"
    }
    Process{
        
        $oAPI.Host = "filetransfer.xxxxxx.com"
        $result = $oAPI.SignOn($Credential.UserName,$Credential.GetNetworkCredential().Password)
        #$result = $oAPI.SignOn("xxxxx",'xxxxx')
       
        if($result -eq $false){
            
            Write-Error -Exception $oAPI.StatusDescription   -Category PermissionDenied -ErrorAction Stop
        }
        $UserGetDetails = $oAPI.UserGetDetails($Email)

        if($UserGetDetails.ID.length -le 0 ){
            Write-Host "New user created"
            $oAPI.UserAddTemporary($Email, "123abc123", $Email, $true) 
        } else{
            Write-Host "User already exist"
        }


        
        $oMsg.RecipientAddUser($Email,'TO')
        $oMsg.Subject = $Subject
        $oMsg.Body = $Body
        $oMsg.AttachmentAdd($Attachment)
        $oAPI.MessageSend([ref]$oMsg)


    }

}


Invoke-SFTPMessage -Email dpeek -Subject "Solidarity files" -Body "Solidarity Setting Files" -Attachment "C:\inetpub\Sites\SOL40Bank\scripts.zip"