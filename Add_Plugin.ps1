﻿<#
.Synopsis
   Add Plugin DLLs and CFGs files to Plugin Folder
.DESCRIPTION
   Add Plugin DLLs and CFGs files to Plugin Folder
.EXAMPLE
   Add-PluginFile -pathToPlugin 'C:\inetpub\Sites\XXX_DNAWeb\Plugins Folder\CheckFreeSBBillPayPlugin-1.0.13.0' -basePath 'C:\inetpub\Sites\'
#>
function Add-PluginFile
{
    [CmdletBinding()]
    Param
    (
        # Path to Plugin folder
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true,ValueFromPipeline=$true)]
        [string] $pathToPlugin = 'C:\inetpub\Sites\XXX_DNAWeb\Plugins Folder\CheckFreeSBBillPayPlugin-1.0.13.0',
        
        #Path to the client csv file. The file list all clients to update to update plugins for
        [Parameter(Mandatory=$false,ValueFromPipelineByPropertyName=$true,ValueFromPipeline=$true)]
        [string]$clientsListName = './clientNames.txt',

        #Based path for sites locations
        [Parameter(Mandatory=$true,ValueFromPipelineByPropertyName=$true,ValueFromPipeline=$true)]
        [string]$basePath = 'C:\inetpub\Sites\'
    )

    
    Process
    {
        # import $clientsListName as csv file
        # Loop through each record and copy the plugin content from $pathToPlugin
        # to client's plugin folder
        Import-Csv $clientsListName -Header ClientList | ForEach-Object {Copy-Item -Path $pathToPlugin\* -Destination "$basePath\$($_.ClientList)\OSI.DNAweb.Web.UI\Plugins"  -Force -Recurse -Container }
    }
    
}