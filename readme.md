##PowerShell Automation Scripts

This repository contains a list of PowerShell scripts and PowerShell modules
used for converting, deconverting and merging online banking clients

1. Add_Cert.ps1
	- Add App Pools to certificate
1. Add_Plugin.ps1
	- Add Plugin DLLs and CFGs files to Plugin Folder
1. SFTP.ps1
	- Send secure email via filetransfer.xxxx.com